package com.example.komaldeepdavuluri.docchat;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MessageListActivity extends AppCompatActivity {

    private RecyclerView mMessageRecycler;
    private MessageListAdapter mMessageAdapter;
    private Button addItemsButton;
    private EditText messageBox;

    String senderString = "komal";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_list);

        final List<UserMessage> llist = new ArrayList<>();
        llist.add(new UserMessage("test", "komal", "today"));
        llist.add(new UserMessage("test", "komal", "today"));
        mMessageRecycler = findViewById(R.id.reyclerview_message_list);
        mMessageRecycler.setHasFixedSize(true);


        mMessageRecycler.setLayoutManager(new LinearLayoutManager(this));

        mMessageAdapter = new MessageListAdapter(this, llist);
        mMessageRecycler.setAdapter(mMessageAdapter);
        mMessageRecycler.smoothScrollToPosition(llist.size());

        messageBox = findViewById(R.id.edittext_chatbox);

        addItemsButton = findViewById(R.id.button_chatbox_send);

        // Instantiate the RequestQueue.
        final RequestQueue queue = Volley.newRequestQueue(this);

        addItemsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String message = messageBox.getText().toString();
                String sender = senderString;

                SimpleDateFormat dtf = new SimpleDateFormat("HH:MM");
                Date date = new Date();
                final String time = dtf.format(date);

                updateMessages(llist, message, "komal", time);

                String url = "https://www.personalityforge.com/api/chat/?apiKey=6nt5d1nJHkqbkphe&message=" +
                        message + "&chatBotID=63906&externalID=chirag1";

                // Request a string response from the provided URL.
                StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String reply = jsonObject.getJSONObject("message").get("message").toString();
                                    if ((int) jsonObject.get("success") == 1) {
                                        updateMessages(llist, reply, "bot", time);
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Failed to send", Toast.LENGTH_LONG).show();
                                    }

                                } catch (JSONException e) {
                                    Toast.makeText(getApplicationContext(), "Data error. Kindly contact admin", Toast.LENGTH_LONG).show();
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Unknown error", Toast.LENGTH_LONG).show();
                    }
                });

                // Add the request to the RequestQueue.
                queue.add(stringRequest);

            }
        });

    }

    private void updateMessages(List<UserMessage> list, String message, String senderString, String time) {
        int listSize = list.size();
        list.add(new UserMessage(message, senderString, time));
        mMessageAdapter.notifyItemRangeInserted(listSize, list.size());
        mMessageRecycler.smoothScrollToPosition(list.size());
    }

}
